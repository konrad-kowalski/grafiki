{literal}
<script id="rfxTimeElementTpl" type="text/html">
    <div>
        {{value}}<span>{{label}}</span>
    </div>
</script>
<script id="pieChartTooltipTpl" type="text/html">
    <div class="chartTooltip" style="background-color: {{color}}">
        <div class="chartName">{{name}}</div>
        <div class="chartValue">{{y}} {{label}}</div>            
    </div>
</script>
{/literal}