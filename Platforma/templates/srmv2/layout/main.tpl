{assign var="action" value=$smarty.get.ac}
{if $smarty.session.nazwa == 'kupiec'}
    {include file="srmv2/partial/newQuickRFXModal.tpl"}
    {include file="srmv2/partial/newRFXModal.tpl"}
    {include file="srmv2/partial/newAuctionsModal.tpl"}
{/if}
<section id="messageBoxContainer"></section>
<main id="mainPageContentBG">
    <div id="mainPageContent">
        {if "srmv2/$action/partial/barmenu.tpl"|template_exists == 1 && $srm_config.parameteres.barmenu}
            {include file="srmv2/$action/partial/barmenu.tpl"}
        {/if}
        {if $srm_config.parameteres.tabmenu}
            {include file="srmv2/partial/maintabs.tpl"}
        {/if}
        {if $srm_config.parameteres.tabpagemenu && $srm_config.parameteres.dashboard && "srmv2/$action/partial/dashboard.tpl"|template_exists == 1}
            {include file="srmv2/$action/partial/dashboard.tpl"}
        {elseif $srm_config.parameteres.tabpagemenu}
            {include file="srmv2/partial/pagetabs.tpl"}
        {/if}        
        {if $srm_config.parameteres.createButton}
            <div id="mainPageContentCreateNew">
                <a href="{$srm_config.parameteres.createButtonURL}" class="actionButton">{'Utwórz'|t}</a>
            </div>    
        {/if}
        {$mainPageContent}  
    </div>
</main>