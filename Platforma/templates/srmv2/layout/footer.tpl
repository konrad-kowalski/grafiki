<footer id="mainPageFooter">
    {if !has_feature('logowanie.web_browser')}
	<div id="fotter">
            <p>&copy; <em>Logintrade</em>.pl</p>
            <p class="right">Created by <a href="http://logintrade.pl">Logintrade.pl</a></p>
	</div>
    {/if}
    {$legacy_footer}
</footer>

{if has_feature('google_analytics.enabled')}
    {include file="srmv2/partial/googleAnalytics.tpl"}
{/if}