/**
 * fbautocomplete jQuery plugin
 * version 1.0 
 *
 * Copyright (c) 2010 Igor Crevar <crewce@hotmail.com>
 *
 * Licensed under the MIT (MIT-LICENSE.txt) 
 *
 * Requires: jquery UI autocomplete plugin, jquery offcourse
 * Based on tutorial from Dan Wellman 
 * http://net.tutsplus.com/tutorials/javascript-ajax/how-to-use-the-jquery-ui-autocomplete-widget/
 * TODO: global functions are bad?
 **/

(function ($) {
    $.fn.fbautocomplete = function (options) {
        var defaultOptions = {
            minLength: 1,
            url: 'friends.php',
            title: 'Remove %s',
            useCache: true,
            formName: 'send_message[user]',
            sendTitles: true,
            appendTo: null,
            dataTag : 'ui-autocomplete',
            width: 266,
            setWidth : false,
            onSelectFunc: function ($obj, btn_remove) {
            },
            onChangeFunc: function ($obj) {
                $obj.val("");
            },
            onRevertFunc: function ($obj) {
            },
            onRemoveFunc: function ($obj, btn_remove) {
                //correct field position
                if ($obj.parent().find('span').length === 0) {
                    //$obj.css("top", 0);
                }
            },
            onRenderFunc: function (ul, item) {
                return $("<li>").append("<a>" + item.label + "</a>").appendTo(ul);
            },
            onAlreadySelected: function ($obj) {
            },
            maxUsers: 0,
            onMaxSelectedFunc: function ($obj) {
            },
            selected: [],
            cache: {},
            showRemoveButton: true
        };
        options = $.extend(true, defaultOptions, options);
        //$(document).unbind();
        this.each(function (i) {
            $(this).fbautocomplete = new $.fbautocomplete($(this), options);
        });


        return this;
    };

    $.fbautocomplete = function ($obj, options) { //constructor
        var $idObj = '#' + $obj.attr('id');
        var $parent = $obj.parent();
        var selected = [];
        var lastXhr;

        for (var i in options.selected) {
            //be sure to use this only if sendTitles is true
            if (typeof options.selected[i].title == 'undefined')
                continue;
            addNewSelected(options.selected[i].id, options.selected[i].title, options.selected[i].not_remove, options.selected[i].soft_remove, options.selected[i].soft_removed_item, options.selected[i].new_item);
        }
        $obj.autocomplete({
            minLength: options.minLength,
            appendTo: options.appendTo,
            source: function (request, response) {
                var term = request.term;
                if (options.useCache)
                {
                    if (term in options.cache) {
                        response($.map(options.cache[ term ], function (item) {
                            return {
                                value: item.title,
                                label: item.title,
                                data: item,
                                id: item.id
                            };
                        }));
                        return;
                    }
                }
                //pass request to server
                lastXhr = $.post(options.url, request, function (data, status, xhr) {
                    if (!$('.ids-fbautocomplete-loader', $obj.parent()).length) {
                        $obj.parent().append('<img src="gfx/ajax-loader.gif" alt="loader" class="ids-fbautocomplete-loader"/>');
                    }
                    if (options.useCache)
                    {
                        options.cache[ term ] = data;
                    }

                    if (lastXhr == xhr)
                    {
                        //parse returned values
                        response($.map(data, function (item) {
                            return {
                                value: item.title,
                                label: item.title,
                                data: item,
                                id: item.id
                            };
                        }));
                    }
                });
            },
            //define select handler
            select: function (e, ui) {
                var remove_btn_obj = addNewSelected(ui.item.id, ui.item.label, 0, 0, 0, 0);
                //custom function
                options.onSelectFunc($obj, remove_btn_obj);
                $obj.val("");
                $(".ids-fbautocomplete-loader", $parent.get(0)).remove();

                //prevent ui updater to set input
                e.preventDefault();
            },
            //define change handler
            change: function (event, ui) {
                options.onChangeFunc($obj);
            },
            open: function(event, ui) {
                options.setWidth ? $(this).autocomplete("widget").css({"width": (options.width+'px')}) : '';
                $(this).autocomplete("widget").css({"z-index": '11000'});
            }
         }).data(options.dataTag)._renderItem = function (ul, item) {
            return options.onRenderFunc(ul, item);
        };

        //add live handler for clicks on remove links
        $(".remove-fbautocomplete", $parent.get(0)).live("click", function (e) {
            e.preventDefault();
            var btn = $(this).parent();
            var btnRemove = $(this);
            options.onRemoveFunc($obj, $(this));
            if (btnRemove.hasClass('soft-remove-fbautocomplete')) {
                btn.css('background-color', '#FFE2E2');
                btnRemove.removeClass('remove-fbautocomplete');
                btnRemove.addClass('revert-fbautocomplete ui-icon ui-icon-arrowreturnthick-1-w');
            } else {
                btn.fadeOut(function () {
                    btn.remove();
                });
            }

            //remove current friend
            var $input = $(this).parent().find('input.ids-fbautocomplete');
            if ($input.length)
                removeSelected($input.val());

        });

        $(".revert-fbautocomplete", $parent.get(0)).live("click", function (e) {
            e.preventDefault();
            var btn = $(this).parent();
            var btnRemove = $(this);
            options.onRevertFunc($obj, $(this));
            if (btnRemove.hasClass('soft-remove-fbautocomplete')) {
                btn.css('background-color', '#f5f5f5');
                btnRemove.removeClass('revert-fbautocomplete ui-icon ui-icon-arrowreturnthick-1-w');
                btnRemove.addClass('remove-fbautocomplete');
            }
        });

        //if user clicks on parent div input is selected  
        $parent.click(function () {

            $obj.focus();
        });

        //Remove loader
        $("body").live('click', function () 
        {
            $(".ids-fbautocomplete-loader").remove();
        });
        
        
        
        function addNewSelected(fId, fTitle, fNotRemove, fSoftRemove, fSoftRemovedItem, fNewItem)
        {
            if (isInSelected(fId)) {
                options.onAlreadySelected($obj);
                return false;
            }
            if (isMaxSelected())
            {
                options.onMaxSelectedFunc($obj);
                return false;
            }
            addToSelected(fId);
            var __title = options.title.replace(/%s/, fTitle);

            var $id_hidden = $('<input type="hidden" />').addClass("ids-fbautocomplete").attr('value', fId);
            if (options.disableData)
            {
                $id_hidden.attr('disabled', 'disabled');
            }

            var $span = $("<span>").html(fTitle).hide().append($id_hidden);
            if (options.sendTitles)
            {
                var $titleHidden = $('<input type="hidden" />').attr('value', fTitle).attr('name', options.formName + '[title][]');
                if (options.disableData)
                {
                    $titleHidden.attr('disabled', 'disabled');
                }

                $span.append($titleHidden);
                $id_hidden.attr('name', options.formName + '[id][]');
            }
            else {
                $id_hidden.attr('name', options.formName + '[]');
            }

            var notRemoveItem = false;
            if (typeof fNotRemove != 'undefined') {
                if (fNotRemove == 1)
                    notRemoveItem = true;
            }

            var softRemove = false;
            if (typeof fSoftRemove != 'undefined') {
                if (fSoftRemove == 1)
                    softRemove = true;
            }

            var softRemovedItem = false;
            if (typeof fSoftRemovedItem != 'undefined') {
                if (fSoftRemovedItem == 1)
                    softRemovedItem = true;
            }

            if (options.showRemoveButton && !notRemoveItem) {
                var $a = $("<a></a>").addClass("remove-fbautocomplete").attr({
                    href: "#",
                    title: __title
                }).text("x").appendTo($span);

                if (softRemove) {
                    $a.addClass('soft-remove-fbautocomplete');

                    if (softRemovedItem) {
                        $a.parent().css('background-color', '#FFE2E2');
                        $a.removeClass('remove-fbautocomplete');
                        $a.addClass('revert-fbautocomplete ui-icon ui-icon-arrowreturnthick-1-w');
                    }
                }
            }

            var newItem = false;
            if (typeof fNewItem != 'undefined') {
                if (fNewItem == 1)
                    newItem = true;
            }
            if (newItem) {
                if (typeof $a == 'undefined') {
                    var $a = $("<a></a>").addClass("remove-fbautocomplete").attr({
                        href: "#",
                        title: __title
                    }).text("x").appendTo($span);
                }
                $a.parent().css('background-color', '#D9FFCC');
            }

            $span.insertBefore($idObj);
            $span.fadeIn();
            return $a;
        }

        function posOfSelected(id)
        {
            for (var i in selected) {
                if (selected[i] == id) {
                    return i;
                }
            }
            return -1;
        }
        function isMaxSelected()
        {
            return options.maxUsers > 0 && selected.length >= options.maxUsers;
        }
        function removeSelected(id)
        {
            var pos = posOfSelected(id);
            if (pos != -1)
                selected.splice(pos, 1);
        }
        function isInSelected(id)
        {
            return posOfSelected(id) != -1;
        }
        function addToSelected(id)
        {
            selected[selected.length] = id;
        }


    };

})(jQuery);

